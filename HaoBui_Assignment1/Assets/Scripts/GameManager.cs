﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public GameObject noTriesLeft;
    public GameObject gameOverLost;
    public GameObject gameOverWon;
    //Called when the countdown reaches 0

    public void GameOverLost()
    {
        Debug.Log("It is too late. Earth is lost!");
        gameOverLost.SetActive(true);
    }
    public void GameOverWon()
    {
        Debug.Log("You are the hero. The invaders have been repelled");
        gameOverWon.SetActive(true);
    }
    public void NoMoreTriesLeft()
    {
        Debug.Log("They have detected us! Run!!!");
        noTriesLeft.SetActive(true);
    }




}
