﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KonamiCode : MonoBehaviour {

	// Use this for initialization
	
      

   

    public List<string> myCodeList = new List<string>();
    public List<KeyCode> KeyList = new List<KeyCode>();

    private void Start()
    {
    

        myCodeList.Add("L");
        myCodeList.Add("R");
        myCodeList.Add("U");
        myCodeList.Add("D");
        myCodeList.Add("A");
        myCodeList.Add("B");

        KeyList.Add(KeyCode.LeftArrow);
        KeyList.Add(KeyCode.RightArrow);
        KeyList.Add(KeyCode.UpArrow);
        KeyList.Add(KeyCode.DownArrow);
        KeyList.Add(KeyCode.A);
        KeyList.Add(KeyCode.B);


        for (int i = 0; i < myCodeList.Count; i++)
        {
            Debug.Log("myCode" + (i + 1) + " value is: " + myCodeList[i]);
            Debug.Log("myKey" + (i + 1) + " value is: " + KeyList[i]);
        }
            

      

    }


}
