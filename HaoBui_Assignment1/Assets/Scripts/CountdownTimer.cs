﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour {

    public GameManager myGameMgr;
    public Text myTimerText;

    public int timerAtStart = 60;  //60, for example
    public int timerCountdownTo = 0; //countdown to 0, for example
    public int criticalTimeValue = 10;//change color when timer is lower than this value
    public Color criticalTimeColor;
    public bool pause = false;

    private float currentTimerVal;

    private bool isCountdownEnded;
    public void PauseButton()
    {
        pause = !pause;
    }
    // initialization
    void Start()
    {

        myTimerText.text = timerAtStart.ToString();
        currentTimerVal = timerAtStart;



    }

    // Update is called once per frame
    void Update()
    {
        if (isCountdownEnded || pause) return;  // prevent the Update function from being executed once we reach the end of timer and once we execute the GameManager.GameOverLost() function call once.

        if (currentTimerVal > timerCountdownTo)
        {
            
            currentTimerVal -= Time.deltaTime;

            myTimerText.text = System.Math.Round(currentTimerVal, 0).ToString();

            if (currentTimerVal < criticalTimeValue)
            {
                myTimerText.color = criticalTimeColor;
              
            }
        }
        else
        {
            isCountdownEnded = true;
            // manage game over 
            myGameMgr.GameOverLost();

        }

    }
}
