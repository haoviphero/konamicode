﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomizerDemo : MonoBehaviour
{
    
    public Text code1;
    public Text code2;
    public Text code3;
    public Text code4;
    public Text code5;
    public Text code6;
    public Text tries;
    public Text statusMessage;
    int currentText = 0;
    int attempt = 0;
    public GameManager myGameMgr;
    public GameObject CountDownManager;

    //Array:

    public List<string> myCodeList = new List<string>();
    public List<KeyCode> KeyList = new List<KeyCode>();

    // random list
    
    public List<string> RandomCodeList = new List<string>();
    public List<KeyCode> RandomKeyList = new List<KeyCode>();

    private void Start()
    {
        KeyList.Add(KeyCode.LeftArrow);
        KeyList.Add(KeyCode.RightArrow);
        KeyList.Add(KeyCode.UpArrow);
        KeyList.Add(KeyCode.DownArrow);
        KeyList.Add(KeyCode.A);
        KeyList.Add(KeyCode.B);


        myCodeList.Add("L");
        myCodeList.Add("R");
        myCodeList.Add("U");
        myCodeList.Add("D");
        myCodeList.Add("A");
        myCodeList.Add("B");

        for (int count = 0; count < 6; count++)
        {

            
            RandomCodeList.Add(myCodeList[Random.Range(0, 5)]);
            RandomKeyList.Add(KeyList[Random.Range(0, 5)]);
        }



    }
    private void Update()
    {
        

        if (Input.GetKeyUp(RandomKeyList[0]))
        {
            Debug.Log("remove Key:" + RandomCodeList[0] + " - Keys Left:" + (RandomCodeList.Count - 1));

            if (currentText == 0)
                code1.text = RandomCodeList[0];
                statusMessage.text = ("1st lock opened");
            if (currentText == 1)
                code2.text = RandomCodeList[0];
            statusMessage.text = ("2nd lock opened");
            if (currentText == 2)
                code3.text = RandomCodeList[0];
            statusMessage.text = ("3rd lock opened");
            if (currentText == 3)
                code4.text = RandomCodeList[0];
            statusMessage.text = ("4th lock opened");
            if (currentText == 4)
                code5.text = RandomCodeList[0];
            statusMessage.text = ("5th lock opened");
            if (currentText == 5)
                code6.text = RandomCodeList[0];
            statusMessage.text = ("Last lock opened");




            RandomCodeList.RemoveAt(0);
            RandomKeyList.RemoveAt(0);

            currentText++;
        }

        else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.B))
        {
            tries.text = (24 - attempt).ToString();
            attempt++;
            Debug.Log("wrong");
            statusMessage.text = ("Access denied");
        }

        // lose
        if (attempt >= 25)
        {
            myGameMgr.NoMoreTriesLeft();
            return;
        }

        // win
        if (RandomCodeList.Count < 1)
        {
            CountDownManager.GetComponent<CountdownTimer>().pause = true;
            myGameMgr.GameOverWon();
            return;
        }

    }
}

